package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"
	pb "github.com/akamemoe/grpcdemo/proto"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Printf("Received: %v", in.GetName())
	return &pb.HelloReply{Message: "Hello " + in.GetName()}, nil
}

type calculator struct{
	pb.UnimplementedCalculatorServer
}

func (c *calculator) Calc(ctx context.Context, t *pb.Task) (*pb.Result,error){
	op := t.GetOperator()
	n1 := t.GetNum1()
	n2 := t.GetNum2()
	switch op {
	case "+":
		return &pb.Result{Result:(n1 + n2),Cost:"fuckyou+"},nil
	case "-":
		return &pb.Result{Result:(n1 - n2),Cost:"fuckyou-"},nil
	case "*":
		return &pb.Result{Result:(n1 * n2),Cost:"fuckyou*"},nil
	case "/":
		return &pb.Result{Result:(n1 / n2),Cost:"fuckyou/"},nil
	default:
		return &pb.Result{Result:0,Cost:"fuckyou0"},nil
	}
}


func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, &server{})
	pb.RegisterCalculatorServer(s, &calculator{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
