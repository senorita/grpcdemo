
package main

import (
	"context"
	"flag"
	"log"
	"time"
	"strings"
	"fmt"
	"bufio"
	"os"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "github.com/akamemoe/grpcdemo/proto"
)

const (
	defaultName = "world"
)

var (
	addr = flag.String("addr", "localhost:50051", "the address to connect to")
	name = flag.String("name", defaultName, "Name to greet")
	n1 = flag.Int("n1", 1, "num1")
	n2 = flag.Int("n2", 1, "num2")
	op = flag.String("op", "+", "operator")
)
func StringPrompt(label string) string {
    var s string
    r := bufio.NewReader(os.Stdin)
    for {
        fmt.Fprint(os.Stdout, label)
        s, _ = r.ReadString('\n')
        if s != "" {
            break
        }
    }
    return strings.TrimSpace(s)
}
func main() {
	flag.Parse()
	// Set up a connection to the server.
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c2 := pb.NewCalculatorClient(conn)


	// Contact the server and print out its response.

	var a int
	var b int
	var o string
	for {
		text := StringPrompt("type expr:")
		if strings.HasPrefix(text,"exit"){
			return
		}
		fmt.Println("your input:",text)
		t := strings.Split(text," ")
		a,_ = strconv.Atoi(t[0])
		o = t[1]
		b,_ = strconv.Atoi(t[2])
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(1) * time.Second)
	defer cancel()
		r2,_ := c2.Calc(ctx,&pb.Task{Operator:o[:1],Num1:int32(a),Num2:int32(b)})
		log.Println("calc:",r2.GetCost(),r2.GetResult())
	}

}
